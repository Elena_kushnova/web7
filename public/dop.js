$(document).ready(function() {
    $(".imgs").slick({
        dots: true,
        infinite: true,
        arrow: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        autoplay: true,
        autoplaySpeed:15000,
        responsive: [{
            breakpoint: 720,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }]
    });
});
